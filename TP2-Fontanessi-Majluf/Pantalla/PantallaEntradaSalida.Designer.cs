﻿namespace Pantalla
{
    partial class PantallaEntradaSalida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtBusqueda = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNombreEmpleadoA = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chkEntrada = new System.Windows.Forms.CheckBox();
            this.chkSalida = new System.Windows.Forms.CheckBox();
            this.DatosAsistencia = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNroLegajo = new System.Windows.Forms.TextBox();
            this.lblNombreEmpleado = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtBusqueda)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Registro";
            this.label1.UseWaitCursor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Nombre empleado:";
            // 
            // dtBusqueda
            // 
            this.dtBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtBusqueda.Location = new System.Drawing.Point(59, 310);
            this.dtBusqueda.Name = "dtBusqueda";
            this.dtBusqueda.Size = new System.Drawing.Size(511, 174);
            this.dtBusqueda.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 271);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Registro de fichajes de:";
            // 
            // lblNombreEmpleadoA
            // 
            this.lblNombreEmpleadoA.AutoSize = true;
            this.lblNombreEmpleadoA.Location = new System.Drawing.Point(180, 271);
            this.lblNombreEmpleadoA.Name = "lblNombreEmpleadoA";
            this.lblNombreEmpleadoA.Size = new System.Drawing.Size(94, 13);
            this.lblNombreEmpleadoA.TabIndex = 26;
            this.lblNombreEmpleadoA.Text = "Nombre Empleado";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(433, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Registrar Entrada/Salida";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(433, 522);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 23);
            this.button2.TabIndex = 28;
            this.button2.Text = "Volver al Menú Principal";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chkEntrada
            // 
            this.chkEntrada.AutoSize = true;
            this.chkEntrada.Location = new System.Drawing.Point(59, 107);
            this.chkEntrada.Name = "chkEntrada";
            this.chkEntrada.Size = new System.Drawing.Size(63, 17);
            this.chkEntrada.TabIndex = 29;
            this.chkEntrada.Text = "Entrada";
            this.chkEntrada.UseVisualStyleBackColor = true;
            this.chkEntrada.CheckedChanged += new System.EventHandler(this.chkEntrada_CheckedChanged);
            // 
            // chkSalida
            // 
            this.chkSalida.AutoSize = true;
            this.chkSalida.Location = new System.Drawing.Point(128, 107);
            this.chkSalida.Name = "chkSalida";
            this.chkSalida.Size = new System.Drawing.Size(55, 17);
            this.chkSalida.TabIndex = 30;
            this.chkSalida.Text = "Salida";
            this.chkSalida.UseVisualStyleBackColor = true;
            this.chkSalida.CheckedChanged += new System.EventHandler(this.chkSalida_CheckedChanged);
            // 
            // DatosAsistencia
            // 
            this.DatosAsistencia.Location = new System.Drawing.Point(59, 145);
            this.DatosAsistencia.Name = "DatosAsistencia";
            this.DatosAsistencia.Size = new System.Drawing.Size(215, 20);
            this.DatosAsistencia.TabIndex = 31;
            this.DatosAsistencia.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Nro Legajo:";
            // 
            // txtNroLegajo
            // 
            this.txtNroLegajo.Location = new System.Drawing.Point(101, 70);
            this.txtNroLegajo.Name = "txtNroLegajo";
            this.txtNroLegajo.Size = new System.Drawing.Size(100, 20);
            this.txtNroLegajo.TabIndex = 33;
            this.txtNroLegajo.TextChanged += new System.EventHandler(this.txtNroLegajo_TextChanged);
            // 
            // lblNombreEmpleado
            // 
            this.lblNombreEmpleado.AutoSize = true;
            this.lblNombreEmpleado.Location = new System.Drawing.Point(323, 77);
            this.lblNombreEmpleado.Name = "lblNombreEmpleado";
            this.lblNombreEmpleado.Size = new System.Drawing.Size(44, 13);
            this.lblNombreEmpleado.TabIndex = 34;
            this.lblNombreEmpleado.Text = "Nombre";
            // 
            // PantallaEntradaSalida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 550);
            this.Controls.Add(this.lblNombreEmpleado);
            this.Controls.Add(this.txtNroLegajo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DatosAsistencia);
            this.Controls.Add(this.chkSalida);
            this.Controls.Add(this.chkEntrada);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblNombreEmpleadoA);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtBusqueda);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "PantallaEntradaSalida";
            this.Text = "Entrada y Salida";
            this.Load += new System.EventHandler(this.PantallaEntradaSalida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtBusqueda)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dtBusqueda;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblNombreEmpleadoA;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkEntrada;
        private System.Windows.Forms.CheckBox chkSalida;
        private System.Windows.Forms.DateTimePicker DatosAsistencia;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNroLegajo;
        private System.Windows.Forms.Label lblNombreEmpleado;
    }
}
﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaBuscarTurno : Form
    {
        Logica.Principal ObjPrincipal = new Logica.Principal();
        public PantallaBuscarTurno()
        {
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscarxDesc_Click(object sender, EventArgs e)
        {
            if (txtBusquedaTurno1.Text == "")
            {
                PantallaBusquedaTurnoDescripcionFalla pantallaBusquedaTurno1 = new PantallaBusquedaTurnoDescripcionFalla();
                pantallaBusquedaTurno1.Show();
            }
            else
            {
                Turno turno = new Turno();
                turno = ObjPrincipal.EncontrarTurnoporDescripcion(txtBusquedaTurno1.Text);
                
                if (turno.DiaLunes != "")
                {
                    chLunes.Checked = true;
                }
                else
                {
                    chLunes.Checked = false;
                }
                if (turno.DiaMartes != "")
                {
                    chMartes.Checked = true;
                }
                else
                {
                    chMartes.Checked = false;
                }
                if (turno.DiaMiercoles != "")
                {
                    chMiercoles.Checked = true;
                }
                else
                {
                    chMiercoles.Checked = false;
                }
                if (turno.DiaJueves != "")
                {
                    chJueves.Checked = true;
                }
                else
                {
                    chJueves.Checked = false;
                }
                if (turno.DiaViernes != "" )
                {
                    chViernes.Checked = true;
                }
                else
                {
                    chViernes.Checked = false;
                }
                if (turno.DiaSabado != "")
                {
                    chSabado.Checked = true;
                }
                else
                {
                    chSabado.Checked = false;
                }
                txtDescripcion.Text = turno.Descripcion;
                txtEstado.Text = turno.Estado.ToString();
                txtHoraDesde.Text = turno.HoraDesde.ToString();
                txtHoraHasta.Text= turno.HoraHasta.ToString();
                txtNroTurno.Text = turno.Numero.ToString();
            }
        }

        private void btnBuscarxDia_Click(object sender, EventArgs e)
        {
            if (txtBusquedaTurno2.Text == "")
            {
                PantallaBusquedaTurnoDiaFalla pantallaBusquedaTurno2 = new PantallaBusquedaTurnoDiaFalla();
                pantallaBusquedaTurno2.Show();
            }
            else
            {
                Turno turno = new Turno();
                turno = ObjPrincipal.EncontrarTurnoporDia(txtBusquedaTurno2.Text);
                if (turno.Numero == 0)
                {
                    PantallaBusquedaTurnoDiaFalla pantallaBusquedaTurno2 = new PantallaBusquedaTurnoDiaFalla();
                    pantallaBusquedaTurno2.Show();
                }
                if (true)
                {

                }
                if (turno.DiaLunes != "")
                {
                    chLunes.Checked = true;
                }
                else
                {
                    chLunes.Checked = false;
                }
                if (turno.DiaMartes != "")
                {
                    chMartes.Checked = true;
                }
                else
                {
                    chMartes.Checked = false;
                }
                if (turno.DiaMiercoles != "")
                {
                    chMiercoles.Checked = true;
                }
                else
                {
                    chMiercoles.Checked = false;
                }
                if (turno.DiaJueves != "")
                {
                    chJueves.Checked = true;
                }
                else
                {
                    chJueves.Checked = false;
                }
                if (turno.DiaViernes != "")
                {
                    chViernes.Checked = true;
                }
                else
                {
                    chViernes.Checked = false;
                }
                if (turno.DiaSabado != "")
                {
                    chSabado.Checked = true;
                }
                else
                {
                    chSabado.Checked = false;
                }
                txtDescripcion.Text = turno.Descripcion;
                txtEstado.Text = turno.Estado.ToString();
                txtHoraDesde.Text = turno.HoraDesde.ToString();
                txtHoraHasta.Text = turno.HoraHasta.ToString();
                txtNroTurno.Text = turno.Numero.ToString();
            }
        }

        private void txtBusquedaTurno1_TextChanged(object sender, EventArgs e)
        {
            if (txtBusquedaTurno1.Text == "")
            {
                dtBusquedaTurno.DataSource = "";
            } //Ver de poner mas validaciones, rompe con un espacio.
            else
            {
                List<Turno> ListaTurnos = new List<Turno>();
                ListaTurnos = ObjPrincipal.EncontrarVariosTurnosporDescripcion(txtBusquedaTurno1.Text);

                dtBusquedaTurno.AutoGenerateColumns = true;
                dtBusquedaTurno.DataSource = ListaTurnos;
            }
        }

        private void txtBusquedaTurno2_TextChanged(object sender, EventArgs e)
        {
            if (txtBusquedaTurno2.Text == "")
            {
                dtBusquedaTurno.DataSource = "";
            } //Ver de poner mas validaciones, rompe con un espacio.
            else
            {
                List<Turno> ListaTurnos = new List<Turno>();
                ListaTurnos = ObjPrincipal.EncontrarVariosTurnosporDia(txtBusquedaTurno2.Text);

                dtBusquedaTurno.AutoGenerateColumns = true;
                dtBusquedaTurno.DataSource = ListaTurnos;
            }
        }

        private void btnNumero_Click(object sender, EventArgs e)
        {
            if (txtNumeroTurno.Text == "")
            {
                PantallaBusquedaTurnoNumeroFalla pantallaBusquedaTurno3 = new PantallaBusquedaTurnoNumeroFalla();
                pantallaBusquedaTurno3.Show();
            }
            else
            {
                Turno turno = new Turno();
                turno = ObjPrincipal.BuscarTurnoPorNro(int.Parse(txtNumeroTurno.Text));
                
                if (turno.DiaLunes != "")
                {
                    chLunes.Checked = true;
                }
                else
                {
                    chLunes.Checked = false;
                }
                if (turno.DiaMartes != "")
                {
                    chMartes.Checked = true;
                }
                else
                {
                    chMartes.Checked = false;
                }
                if (turno.DiaMiercoles != "")
                {
                    chMiercoles.Checked = true;
                }
                else
                {
                    chMiercoles.Checked = false;
                }
                if (turno.DiaJueves != "")
                {
                    chJueves.Checked = true;
                }
                else
                {
                    chJueves.Checked = false;
                }
                if (turno.DiaViernes != "")
                {
                    chViernes.Checked = true;
                }
                else
                {
                    chViernes.Checked = false;
                }
                if (turno.DiaSabado != "")
                {
                    chSabado.Checked = true;
                }
                else
                {
                    chSabado.Checked = false;
                }
                txtDescripcion.Text = turno.Descripcion;
                txtEstado.Text = turno.Estado.ToString();
                txtHoraDesde.Text = turno.HoraDesde.ToString();
                txtHoraHasta.Text = turno.HoraHasta.ToString();
                txtNroTurno.Text = turno.Numero.ToString();
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            string DiaLunes, DiaMartes, DiaMiercoles, DiaJueves, DiaViernes, DiaSabado;
            if (chLunes.Checked == true)
            {
                DiaLunes = "Monday";
            }
            else
            {
                DiaLunes = "";
            }
            if (chMartes.Checked == true)
            {
                DiaMartes = "Tuesday";
            }
            else
            {
                DiaMartes = "";
            }
            if (chMiercoles.Checked == true)
            {
                DiaMiercoles = "Wednesday";
            }
            else
            {
                DiaMiercoles = "";
            }
            if (chJueves.Checked == true)
            {
                DiaJueves = "Thursday";
            }
            else
            {
                DiaJueves = "";
            }
            if (chViernes.Checked == true)
            {
                DiaViernes = "Friday";
            }
            else
            {
                DiaViernes = "";
            }
            if (chSabado.Checked == true)
            {
                DiaSabado = "Saturday";
            }
            else
            {
                DiaSabado = "";
            }
            if (txtDescripcion == null | (chLunes.Checked == false & chMartes.Checked == false & chMiercoles.Checked == false & chJueves.Checked == false & chViernes.Checked == false & chSabado.Checked == false) | txtHoraDesde == null | txtHoraHasta == null | txtEstado == null)
            {
                PantallaModificarTurnoFallo pantallaModificarTurnoFallo1 = new PantallaModificarTurnoFallo();
                pantallaModificarTurnoFallo1.Show();
            }
            else
            {

                if (ObjPrincipal.ModificarTurno(int.Parse(txtNroTurno.Text), txtDescripcion.Text, DiaLunes,DiaMartes,DiaMiercoles,DiaJueves,DiaViernes,DiaSabado, double.Parse(txtHoraDesde.Text), double.Parse(txtHoraHasta.Text)))
                {
                    PantallaModificarTurnoExitoso pantallaModificarTurnoExitoso = new PantallaModificarTurnoExitoso();
                    pantallaModificarTurnoExitoso.Show();
                }
                else
                {
                    PantallaModificarTurnoFallo pantallaModificarTurnoFallo2 = new PantallaModificarTurnoFallo();
                    pantallaModificarTurnoFallo2.Show();
                }
            }
            VaciarTextboxes();
        }
        private void VaciarTextboxes()
        {
            txtNroTurno.Text = "";
            txtDescripcion.Text = "";
            chLunes.Checked = false;
            chMartes.Checked = false;
            chMiercoles.Checked = false;
            chJueves.Checked = false;
            chViernes.Checked = false;
            chSabado.Checked = false;
            txtHoraDesde.Text = "";
            txtHoraHasta.Text = "";
            txtEstado.Text = "";
            
            //chkEstado.Enabled = true;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            ObjPrincipal.EliminarTurnoPorNumero(int.Parse(txtNumeroTurno.Text));
            PantallaEliminarTurnoExitoso pantallaEliminarTurnoExitoso = new PantallaEliminarTurnoExitoso();
            pantallaEliminarTurnoExitoso.Show();
            
            VaciarTextboxes();
        }
    }
}

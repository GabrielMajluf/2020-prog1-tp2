﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaRegistrarEmpleado : Form
    {
        Logica.Principal principal = new Logica.Principal();
        public PantallaRegistrarEmpleado()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (txtNombre.Text == "" | txtDomicilio.Text == "" | txtLocalidad.Text == "" | txtSueldoNeto.Text == "" | txtNroTurno.Text == "")
            {
                PantallaRegistroEmpleadoFallido pantallaRegistroEmpleadoFallido = new PantallaRegistroEmpleadoFallido();
                pantallaRegistroEmpleadoFallido.Show();
            }
            else
            {
                if (txtTelefono.Text == "")
                {
                    txtTelefono.Text = "0";
                }
                if (txtEmail.Text == "")
                {
                    txtEmail.Text = "No ingresó";
                }
                
                if (principal.AgregarEmpleado(txtNombre.Text,txtDomicilio.Text,txtLocalidad.Text,int.Parse(txtTelefono.Text),txtEmail.Text,float.Parse(txtSueldoNeto.Text), int.Parse(txtNroTurno.Text)))
                {
                    PantallaRegistroEmpleadoExitoso pantallaRegistroEmpleadoExitoso = new PantallaRegistroEmpleadoExitoso();
                    pantallaRegistroEmpleadoExitoso.Show();
                }
                else
                {
                    PantallaRegistroEmpleadoFallido pantallaRegistroEmpleadoFallido = new PantallaRegistroEmpleadoFallido();
                    pantallaRegistroEmpleadoFallido.Show();
                }
            }
        }
    }
}

﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaInformeEmpleados : Form
    {
        Logica.Principal ObjPrincipal = new Logica.Principal();
        public PantallaInformeEmpleados()
        {
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PantallaInformeEmpleados_Load(object sender, EventArgs e)
        {
            List<Informe> ListaInformes = new List<Informe>();
            ListaInformes = ObjPrincipal.InformarEmpleados();
            dtInformeEmpleados.AutoGenerateColumns = true;
            dtInformeEmpleados.DataSource = ListaInformes;

        }
    }
}

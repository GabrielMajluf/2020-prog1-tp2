﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaInformeFichajes : Form
    {
        Logica.Principal ObjPrincipal = new Logica.Principal();
        public PantallaInformeFichajes()
        {
            InitializeComponent();
        }

        private void btnBuscarNroLeg_Click(object sender, EventArgs e)
        {
            if (txtNroLegajo.Text == "")
            {
                PantallaBusquedaInformeFalla pantallaBusquedaInformeFalla = new PantallaBusquedaInformeFalla();
                pantallaBusquedaInformeFalla.Show();
            }
            else
            {
                List<Asistencia> ListaAsistencia = new List<Asistencia>();
                ListaAsistencia = ObjPrincipal.BuscarListadoAsistenciaEmpleadoPorNroLegajo(int.Parse(txtNroLegajo.Text));
                ListaAsistencia = ListaAsistencia.OrderByDescending(x => x.FechayHoraIngreso).ToList();
                dtFichajes.AutoGenerateColumns = true;
                dtFichajes.DataSource = ListaAsistencia;
            }
        }

        private void btnBuscarFechas_Click(object sender, EventArgs e)
        {
            if (txtNroLegajo.Text == "" | dtFechaDesde.Value == null | dtFechaHasta.Value == null)
            {
                PantallaBusquedaInformeFalla pantallaBusquedaInformeFalla = new PantallaBusquedaInformeFalla();
                pantallaBusquedaInformeFalla.Show();
            }
            else
            {
                List<Asistencia> ListaAsistencia = new List<Asistencia>();
                ListaAsistencia = ObjPrincipal.BuscarListadoAsistenciaEmpleadoPorFechas(int.Parse(txtNroLegajo.Text), dtFechaDesde.Value, dtFechaHasta.Value);
                ListaAsistencia = ListaAsistencia.OrderByDescending(x => x.FechayHoraIngreso).ToList();
                dtFichajes.AutoGenerateColumns = true;
                dtFichajes.DataSource = ListaAsistencia;
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

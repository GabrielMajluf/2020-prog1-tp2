﻿namespace Pantalla
{
    partial class PantallaRegistrarTurno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVolverMenu = new System.Windows.Forms.Button();
            this.btnRegistrarTurno = new System.Windows.Forms.Button();
            this.txtHsHasta = new System.Windows.Forms.TextBox();
            this.txtHsDesde = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chLunes = new System.Windows.Forms.CheckBox();
            this.chMartes = new System.Windows.Forms.CheckBox();
            this.chMiercoles = new System.Windows.Forms.CheckBox();
            this.chJueves = new System.Windows.Forms.CheckBox();
            this.chViernes = new System.Windows.Forms.CheckBox();
            this.chSabado = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnVolverMenu
            // 
            this.btnVolverMenu.Location = new System.Drawing.Point(366, 104);
            this.btnVolverMenu.Name = "btnVolverMenu";
            this.btnVolverMenu.Size = new System.Drawing.Size(129, 25);
            this.btnVolverMenu.TabIndex = 38;
            this.btnVolverMenu.Text = "Volver al Menú Principal";
            this.btnVolverMenu.UseVisualStyleBackColor = true;
            this.btnVolverMenu.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnRegistrarTurno
            // 
            this.btnRegistrarTurno.Location = new System.Drawing.Point(366, 75);
            this.btnRegistrarTurno.Name = "btnRegistrarTurno";
            this.btnRegistrarTurno.Size = new System.Drawing.Size(129, 25);
            this.btnRegistrarTurno.TabIndex = 37;
            this.btnRegistrarTurno.Text = "Registrar turno";
            this.btnRegistrarTurno.UseVisualStyleBackColor = true;
            this.btnRegistrarTurno.Click += new System.EventHandler(this.btnRegistrarTurno_Click);
            // 
            // txtHsHasta
            // 
            this.txtHsHasta.Location = new System.Drawing.Point(96, 276);
            this.txtHsHasta.Name = "txtHsHasta";
            this.txtHsHasta.Size = new System.Drawing.Size(182, 20);
            this.txtHsHasta.TabIndex = 32;
            // 
            // txtHsDesde
            // 
            this.txtHsDesde.Location = new System.Drawing.Point(96, 246);
            this.txtHsDesde.Name = "txtHsDesde";
            this.txtHsDesde.Size = new System.Drawing.Size(182, 20);
            this.txtHsDesde.TabIndex = 31;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(96, 77);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(182, 20);
            this.txtDescripcion.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Hora Desde:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Hora Hasta:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Día:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Descripcion:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 25);
            this.label1.TabIndex = 20;
            this.label1.Text = "Completar los siguientes campos";
            this.label1.UseWaitCursor = true;
            // 
            // chLunes
            // 
            this.chLunes.AutoSize = true;
            this.chLunes.Location = new System.Drawing.Point(96, 105);
            this.chLunes.Name = "chLunes";
            this.chLunes.Size = new System.Drawing.Size(55, 17);
            this.chLunes.TabIndex = 39;
            this.chLunes.Text = "Lunes";
            this.chLunes.UseVisualStyleBackColor = true;
            // 
            // chMartes
            // 
            this.chMartes.AutoSize = true;
            this.chMartes.Location = new System.Drawing.Point(96, 129);
            this.chMartes.Name = "chMartes";
            this.chMartes.Size = new System.Drawing.Size(58, 17);
            this.chMartes.TabIndex = 40;
            this.chMartes.Text = "Martes";
            this.chMartes.UseVisualStyleBackColor = true;
            // 
            // chMiercoles
            // 
            this.chMiercoles.AutoSize = true;
            this.chMiercoles.Location = new System.Drawing.Point(96, 153);
            this.chMiercoles.Name = "chMiercoles";
            this.chMiercoles.Size = new System.Drawing.Size(71, 17);
            this.chMiercoles.TabIndex = 41;
            this.chMiercoles.Text = "Miercoles";
            this.chMiercoles.UseVisualStyleBackColor = true;
            // 
            // chJueves
            // 
            this.chJueves.AutoSize = true;
            this.chJueves.Location = new System.Drawing.Point(96, 177);
            this.chJueves.Name = "chJueves";
            this.chJueves.Size = new System.Drawing.Size(60, 17);
            this.chJueves.TabIndex = 42;
            this.chJueves.Text = "Jueves";
            this.chJueves.UseVisualStyleBackColor = true;
            // 
            // chViernes
            // 
            this.chViernes.AutoSize = true;
            this.chViernes.Location = new System.Drawing.Point(96, 201);
            this.chViernes.Name = "chViernes";
            this.chViernes.Size = new System.Drawing.Size(61, 17);
            this.chViernes.TabIndex = 43;
            this.chViernes.Text = "Viernes";
            this.chViernes.UseVisualStyleBackColor = true;
            // 
            // chSabado
            // 
            this.chSabado.AutoSize = true;
            this.chSabado.Location = new System.Drawing.Point(96, 223);
            this.chSabado.Name = "chSabado";
            this.chSabado.Size = new System.Drawing.Size(63, 17);
            this.chSabado.TabIndex = 44;
            this.chSabado.Text = "Sabado";
            this.chSabado.UseVisualStyleBackColor = true;
            // 
            // PantallaRegistrarTurno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 306);
            this.Controls.Add(this.chSabado);
            this.Controls.Add(this.chViernes);
            this.Controls.Add(this.chJueves);
            this.Controls.Add(this.chMiercoles);
            this.Controls.Add(this.chMartes);
            this.Controls.Add(this.chLunes);
            this.Controls.Add(this.btnVolverMenu);
            this.Controls.Add(this.btnRegistrarTurno);
            this.Controls.Add(this.txtHsHasta);
            this.Controls.Add(this.txtHsDesde);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "PantallaRegistrarTurno";
            this.Text = "Registrar turno";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVolverMenu;
        private System.Windows.Forms.Button btnRegistrarTurno;
        private System.Windows.Forms.TextBox txtHsHasta;
        private System.Windows.Forms.TextBox txtHsDesde;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chLunes;
        private System.Windows.Forms.CheckBox chMartes;
        private System.Windows.Forms.CheckBox chMiercoles;
        private System.Windows.Forms.CheckBox chJueves;
        private System.Windows.Forms.CheckBox chViernes;
        private System.Windows.Forms.CheckBox chSabado;
    }
}
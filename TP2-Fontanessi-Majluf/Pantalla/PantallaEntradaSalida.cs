﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaEntradaSalida : Form
    {
        Logica.Principal ObjPrincipal = new Logica.Principal();
        public PantallaEntradaSalida()
        {
            InitializeComponent();
        }

        private void PantallaEntradaSalida_Load(object sender, EventArgs e)
        {
         
        }

        private void chkEntrada_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEntrada.Checked)
            {
                chkSalida.Enabled = false;
            }
            else
            {
                chkSalida.Enabled = true;
            }
        }

        private void chkSalida_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSalida.Checked)
            {
                chkEntrada.Enabled = false;
            }
            else
            {
                chkEntrada.Enabled = true;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtNroLegajo.Text == "" )
            {
                PantallaEntradaSalidaError pantallaEntradaSalidaError = new PantallaEntradaSalidaError();
                pantallaEntradaSalidaError.Show();
            }
            else
            {
                if (chkEntrada.Checked == false & chkSalida.Checked == false )
                {
                    PantallaEntradaSalidaError pantallaEntradaSalidaError = new PantallaEntradaSalidaError();
                    pantallaEntradaSalidaError.Show();
                }
                else
                {
                    
                    if (chkEntrada.Checked )
                    {
                        Empleado empleado = new Empleado();
                        empleado = ObjPrincipal.EncontrarEmpleadoporLegajo(int.Parse(txtNroLegajo.Text));
                        lblNombreEmpleado.Text = empleado.Nombre;
                        lblNombreEmpleadoA.Text = empleado.Nombre;
                        if (ObjPrincipal.RegistrarIngresoEmpleado(DatosAsistencia.Value, empleado))
                        {

                            PantallaEntradaSalidaExito pantallaEntradaSalidaExito = new PantallaEntradaSalidaExito();
                            pantallaEntradaSalidaExito.Show();
                        }
                        else
                        {
                            PantallaEntradaSalidaError pantallaEntradaSalidaError = new PantallaEntradaSalidaError();
                            pantallaEntradaSalidaError.Show();
                        }
                        
                    }
                    else
                    {
                        if (chkSalida.Checked)
                        {
                            Empleado empleado = new Empleado();
                            empleado = ObjPrincipal.EncontrarEmpleadoporLegajo(int.Parse(txtNroLegajo.Text));
                            lblNombreEmpleado.Text = empleado.Nombre;
                            lblNombreEmpleadoA.Text = empleado.Nombre;
                            if (ObjPrincipal.RegistrarEgresoEmpleado(DatosAsistencia.Value, empleado))
                            {
                                PantallaEntradaSalidaExito pantallaEntradaSalidaExito = new PantallaEntradaSalidaExito();
                                pantallaEntradaSalidaExito.Show();
                            }
                            else
                            {
                                PantallaEntradaSalidaError pantallaEntradaSalidaError = new PantallaEntradaSalidaError();
                                pantallaEntradaSalidaError.Show();
                            }
                        }
                       
                    }
                }
            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNroLegajo_TextChanged(object sender, EventArgs e)
        {
            if (txtNroLegajo.Text == "")
            {

            }
            else
            {
                if (txtNroLegajo.Text == " ")
                {

                }
                else
                {
                    List<Asistencia> ListaAsistencia= new List<Asistencia>();
                    ListaAsistencia = ObjPrincipal.BuscarListadoAsistenciaEmpleadoPorNroLegajo(int.Parse(txtNroLegajo.Text));


                    dtBusqueda.AutoGenerateColumns = true;
                    dtBusqueda.DataSource = ListaAsistencia;

                }
            }
        }
    }
}

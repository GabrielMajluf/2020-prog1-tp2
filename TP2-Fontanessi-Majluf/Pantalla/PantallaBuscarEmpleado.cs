﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaBuscarEmpleado : Form
    {
        Logica.Principal ObjPrincipal = new Logica.Principal();
        public PantallaBuscarEmpleado()
        {
            InitializeComponent();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (lblNroLegajo.Text != "XXXX")
            {
                ObjPrincipal.EliminarEmpleadoPorLegajo(int.Parse(lblNroLegajo.Text));
                PantallaEliminarEmpleadoExitoso pantallaEliminarEmpleadoExitoso = new PantallaEliminarEmpleadoExitoso();
                pantallaEliminarEmpleadoExitoso.Show();
            }
            VaciarTextboxes();
            
        }

        private void btnBuscarNroLegajo_Click(object sender, EventArgs e)
        {
            if (txtNroLegajo.Text == "")
            {
                PantallaBuscarNroEmpleadoFalla pantallaBuscarNroEmpleadoFalla = new PantallaBuscarNroEmpleadoFalla();
                pantallaBuscarNroEmpleadoFalla.Show();
            }
            else
            {
                
                Empleado empleado = new Empleado();
                empleado = ObjPrincipal.EncontrarEmpleadoporLegajo(int.Parse(txtNroLegajo.Text));
                if (empleado.Legajo != 0)
                {
                    // txtApellidoE.Text = empleado.Apellido;
                    txtDomicilio.Text = empleado.Domicilio;
                    txtEmail.Text = empleado.CorreoElectronico;
                    txtNombreE.Text = empleado.Nombre;
                    txtSueldoNeto.Text = empleado.SueldoNeto.ToString();
                    txtLocalidad.Text = empleado.Localidad;
                    lblNroLegajo.Text = empleado.Legajo.ToString();
                    txtNroTurno.Text = empleado.TurnoAsignado.ToString();
                    txtTelefono.Text = empleado.Telefono.ToString();
                    if (empleado.Estado == 0)
                    {
                        chkEstado.Checked = true;
                        chkEstado.Enabled = false;
                    }
                    else
                    {
                        chkEstado.Checked = false;
                        chkEstado.Enabled = false;
                    }
                    lblFechaIngreso.Text = empleado.FechaIngreso.ToString();
                    if (empleado.FechaFinActividad != null)
                    {
                        lblFechaFinActividad.Text = empleado.FechaFinActividad.ToString();
                    }
                    else
                    {
                        lblFechaFinActividad.Text = "En actividad";
                    }
                }
                else
                {
                    PantallaBusquedaError pantallaBusquedaError = new PantallaBusquedaError();
                    pantallaBusquedaError.Show();
                }
               
            }
           
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if ( txtDomicilio == null | txtNombreE.Text == null | txtLocalidad.Text == null | txtSueldoNeto == null | txtNroTurno == null)
            {
                PantallaModificarEmpleadoFallo pantallaModificarEmpleadoFallo1 = new PantallaModificarEmpleadoFallo();
                pantallaModificarEmpleadoFallo1.Show();
            }
            else
            {            
                int Estado;
                if (chkEstado.Checked == true)
                {
                    Estado = 0;
                }
                else
                {
                    Estado = 99;
                }
                
                if (ObjPrincipal.ModificarEmpleado(int.Parse(lblNroLegajo.Text),txtNombreE.Text,txtDomicilio.Text,txtLocalidad.Text,int.Parse(txtTelefono.Text),txtEmail.Text,float.Parse(txtSueldoNeto.Text),int.Parse(txtNroTurno.Text),Estado))
                {
                    PantallaModificarEmpleadoExitoso pantallaModificarEmpleadoExitoso = new PantallaModificarEmpleadoExitoso();
                    pantallaModificarEmpleadoExitoso.Show();
                }
                else
                {
                    PantallaModificarEmpleadoFallo pantallaModificarEmpleadoFallo = new PantallaModificarEmpleadoFallo();
                    pantallaModificarEmpleadoFallo.Show();
                }
                VaciarTextboxes();
            }            
        }

        private void txtNroLegajo_TextChanged(object sender, EventArgs e)
        {
            if (txtNroLegajo.Text == "")
            {
                dtBusqueda.DataSource = "";
            } //Ver de poner mas validaciones, rompe con un espacio.
            else
            {
                if (txtNroLegajo.Text == " ")
                {
                    dtBusqueda.DataSource = "";
                }
                else
                {
                    List<Empleado> ListaEmpleados = new List<Empleado>();
                    ListaEmpleados = ObjPrincipal.RellenarGridEmpleado(int.Parse(txtNroLegajo.Text));


                    dtBusqueda.AutoGenerateColumns = true;
                    dtBusqueda.DataSource = ListaEmpleados;
                }
              
            }            
            //  DataGridView1.AutoGenerateColumns = true;
            //DataGridView1.DataSource = ListaNueva;
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            if (txtNombre.Text != "")
            {
                List<Empleado> ListaEmpleados = new List<Empleado>();
                ListaEmpleados = ObjPrincipal.EncontrarVariosEmpleadosporNombre(txtNombre.Text);


                dtBusqueda.AutoGenerateColumns = true;
                dtBusqueda.DataSource = ListaEmpleados;
            } 
            else
            {
                if (txtNombre.Text == "")
                {
                    dtBusqueda.DataSource = "";
                }
                else
                {
                    dtBusqueda.DataSource = "";
                }
                
            }
        }
        private void VaciarTextboxes()
        {
            lblNroLegajo.Text = "";
            txtDomicilio.Text = "";
            txtEmail.Text = ""; 
            txtLocalidad.Text = "";
            txtNombreE.Text = "";
            txtNroTurno.Text = "";
            txtSueldoNeto.Text = "";
            txtTelefono.Text = "";
            chkEstado.Enabled = true;
        }

        private void btnBuscarNombre_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "")
            {

            }
            else
            {
                Empleado empleado = new Empleado();
                empleado = ObjPrincipal.EncontrarEmpleadoPorNombre(txtNombre.Text);
                if (empleado.Legajo != 0)
                {
                    txtDomicilio.Text = empleado.Domicilio;
                    txtEmail.Text = empleado.CorreoElectronico;
                    txtNombreE.Text = empleado.Nombre;
                    txtSueldoNeto.Text = empleado.SueldoNeto.ToString();
                    txtLocalidad.Text = empleado.Localidad;
                    lblNroLegajo.Text = empleado.Legajo.ToString();
                    txtNroTurno.Text = empleado.TurnoAsignado.ToString();
                    txtTelefono.Text = empleado.Telefono.ToString();
                    if (empleado.Estado == 0)
                    {
                        chkEstado.Checked = true;
                        chkEstado.Enabled = false;
                    }
                    else
                    {
                        chkEstado.Checked = false;
                        chkEstado.Enabled = false;
                    }
                    lblFechaIngreso.Text = empleado.FechaIngreso.ToString();
                    if (empleado.FechaFinActividad != null)
                    {
                        lblFechaFinActividad.Text = empleado.FechaFinActividad.ToString();
                    }
                    else
                    {
                        lblFechaFinActividad.Text = "En actividad";
                    }
                }
                else
                {
                    PantallaBusquedaError pantallaBusquedaError = new PantallaBusquedaError();
                    pantallaBusquedaError.Show();
                }
               
            }

        }
    }
}

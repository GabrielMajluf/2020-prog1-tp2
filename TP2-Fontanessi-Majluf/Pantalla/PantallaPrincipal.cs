﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void registrarEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaRegistrarEmpleado pantallaRegistrarEmpleado = new PantallaRegistrarEmpleado();
            pantallaRegistrarEmpleado.Show();
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {
            Logica.DatosArchivos DatosArchivos = new Logica.DatosArchivos();
            DatosArchivos.InicializarArchivos();
            Logica.Principal principal = new Logica.Principal();
            principal.RellenarListas();
        }

        private void crearTurnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaRegistrarTurno pantallaRegistrarTurno = new PantallaRegistrarTurno();
            pantallaRegistrarTurno.Show();
        }

        private void buscarEmpleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaBuscarEmpleado pantallaBuscarEmpleado = new PantallaBuscarEmpleado();
            pantallaBuscarEmpleado.Show();
        }

        private void buscarTurnoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaBuscarTurno pantallaBuscarTurno = new PantallaBuscarTurno();
            pantallaBuscarTurno.Show();
        }

        private void entradasYSalidasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaEntradaSalida pantallaEntradaSalida = new PantallaEntradaSalida();
            pantallaEntradaSalida.Show();
        }

        private void registroDeFichajesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaInformeFichajes pantallaInformeFichajes = new PantallaInformeFichajes();
            pantallaInformeFichajes.Show();

        }

        private void empleadosDeLaEmpresaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaInformeEmpleados pantallaInformeEmpleados = new PantallaInformeEmpleados();
            pantallaInformeEmpleados.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla
{
    public partial class PantallaRegistrarTurno : Form
    {
        Logica.Principal ObjPrincipal = new Logica.Principal();
        public PantallaRegistrarTurno()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrarTurno_Click(object sender, EventArgs e)
        {
            string DiaLunes, DiaMartes, DiaMiercoles, DiaJueves, DiaViernes, DiaSabado;
            if (chLunes.Checked == true)
            {
                DiaLunes = "Monday";
            }
            else
            {
                DiaLunes = "";
            }
            if (chMartes.Checked==true)
            {
                DiaMartes = "Tuesday";
            }
            else
            {
                DiaMartes = "";
            }
            if (chMiercoles.Checked == true)
            {
                DiaMiercoles = "Wednesday";
            }
            else
            {
                DiaMiercoles = "";
            }
            if (chJueves.Checked == true)
            {
                DiaJueves = "Thursday";
            }
            else
            {
                DiaJueves = "";
            }
            if (chViernes.Checked == true)
            {
                DiaViernes = "Friday";
            }
            else
            {
                DiaViernes = "";
            }
            if (chSabado.Checked == true)
            {
                DiaSabado = "Saturday";
            }
            else
            {
                DiaSabado = "";
            }
            if (txtDescripcion.Text == "" | (chLunes.Checked == false & chMartes.Checked == false & chMiercoles.Checked == false & chJueves.Checked == false & chViernes.Checked == false & chSabado.Checked == false)| txtHsDesde.Text == "" | txtHsHasta.Text == "")
            {
                PantallaRegistroTurnoFallido RegistroTurnoFallido = new PantallaRegistroTurnoFallido();
                RegistroTurnoFallido.Show();
            }
            else
            {
                ObjPrincipal.AgregarTurno(txtDescripcion.Text,DiaLunes, DiaMartes,DiaMiercoles,DiaJueves,DiaViernes,DiaSabado, double.Parse(txtHsDesde.Text), double.Parse(txtHsHasta.Text));
                PantallaRegistrarTurnoExitoso RegistroTurnoExitoso = new PantallaRegistrarTurnoExitoso();
                RegistroTurnoExitoso.Show();
            }
        }
    }
}

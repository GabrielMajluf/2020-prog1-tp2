﻿namespace Pantalla
{
    partial class PantallaBuscarTurno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label12 = new System.Windows.Forms.Label();
            this.btnBuscarxDia = new System.Windows.Forms.Button();
            this.btnBuscarxDesc = new System.Windows.Forms.Button();
            this.txtBusquedaTurno2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBusquedaTurno1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.txtHoraHasta = new System.Windows.Forms.TextBox();
            this.txtHoraDesde = new System.Windows.Forms.TextBox();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNroTurno = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnVolver = new System.Windows.Forms.Button();
            this.dtBusquedaTurno = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.btnNumero = new System.Windows.Forms.Button();
            this.txtNumeroTurno = new System.Windows.Forms.TextBox();
            this.chSabado = new System.Windows.Forms.CheckBox();
            this.chViernes = new System.Windows.Forms.CheckBox();
            this.chJueves = new System.Windows.Forms.CheckBox();
            this.chMiercoles = new System.Windows.Forms.CheckBox();
            this.chMartes = new System.Windows.Forms.CheckBox();
            this.chLunes = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtBusquedaTurno)).BeginInit();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(292, 25);
            this.label12.TabIndex = 54;
            this.label12.Text = "Datos obtenidos de la búsqueda";
            this.label12.UseWaitCursor = true;
            // 
            // btnBuscarxDia
            // 
            this.btnBuscarxDia.Location = new System.Drawing.Point(349, 100);
            this.btnBuscarxDia.Name = "btnBuscarxDia";
            this.btnBuscarxDia.Size = new System.Drawing.Size(119, 23);
            this.btnBuscarxDia.TabIndex = 53;
            this.btnBuscarxDia.Text = "Buscar por Día";
            this.btnBuscarxDia.UseVisualStyleBackColor = true;
            this.btnBuscarxDia.Click += new System.EventHandler(this.btnBuscarxDia_Click);
            // 
            // btnBuscarxDesc
            // 
            this.btnBuscarxDesc.Location = new System.Drawing.Point(349, 56);
            this.btnBuscarxDesc.Name = "btnBuscarxDesc";
            this.btnBuscarxDesc.Size = new System.Drawing.Size(119, 23);
            this.btnBuscarxDesc.TabIndex = 52;
            this.btnBuscarxDesc.Text = "Buscar Descripción";
            this.btnBuscarxDesc.UseVisualStyleBackColor = true;
            this.btnBuscarxDesc.Click += new System.EventHandler(this.btnBuscarxDesc_Click);
            // 
            // txtBusquedaTurno2
            // 
            this.txtBusquedaTurno2.Location = new System.Drawing.Point(122, 102);
            this.txtBusquedaTurno2.Name = "txtBusquedaTurno2";
            this.txtBusquedaTurno2.Size = new System.Drawing.Size(182, 20);
            this.txtBusquedaTurno2.TabIndex = 51;
            this.txtBusquedaTurno2.TextChanged += new System.EventHandler(this.txtBusquedaTurno2_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(88, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 50;
            this.label11.Text = "Día:";
            // 
            // txtBusquedaTurno1
            // 
            this.txtBusquedaTurno1.Location = new System.Drawing.Point(122, 58);
            this.txtBusquedaTurno1.Name = "txtBusquedaTurno1";
            this.txtBusquedaTurno1.Size = new System.Drawing.Size(182, 20);
            this.txtBusquedaTurno1.TabIndex = 49;
            this.txtBusquedaTurno1.TextChanged += new System.EventHandler(this.txtBusquedaTurno1_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(50, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 48;
            this.label10.Text = "Descripción:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 25);
            this.label1.TabIndex = 47;
            this.label1.Text = "Búsqueda";
            this.label1.UseWaitCursor = true;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(640, 450);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(129, 25);
            this.btnEliminar.TabIndex = 64;
            this.btnEliminar.Text = "Eliminar Turno";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(640, 421);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(129, 25);
            this.btnModificar.TabIndex = 63;
            this.btnModificar.Text = "Modificar Turno";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // txtHoraHasta
            // 
            this.txtHoraHasta.Location = new System.Drawing.Point(411, 453);
            this.txtHoraHasta.Name = "txtHoraHasta";
            this.txtHoraHasta.Size = new System.Drawing.Size(182, 20);
            this.txtHoraHasta.TabIndex = 62;
            // 
            // txtHoraDesde
            // 
            this.txtHoraDesde.Location = new System.Drawing.Point(411, 392);
            this.txtHoraDesde.Name = "txtHoraDesde";
            this.txtHoraDesde.Size = new System.Drawing.Size(182, 20);
            this.txtHoraDesde.TabIndex = 61;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(122, 453);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(182, 20);
            this.txtDescripcion.TabIndex = 59;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(343, 395);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 58;
            this.label6.Text = "Hora Desde:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(343, 456);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 57;
            this.label5.Text = "Hora Hasta:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(50, 456);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Descripcion:";
            // 
            // txtNroTurno
            // 
            this.txtNroTurno.Location = new System.Drawing.Point(122, 395);
            this.txtNroTurno.Name = "txtNroTurno";
            this.txtNroTurno.Size = new System.Drawing.Size(182, 20);
            this.txtNroTurno.TabIndex = 66;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 398);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 65;
            this.label4.Text = "Numero Turno:";
            // 
            // txtEstado
            // 
            this.txtEstado.Location = new System.Drawing.Point(411, 504);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(182, 20);
            this.txtEstado.TabIndex = 68;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(362, 507);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "Estado:";
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(640, 479);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(129, 25);
            this.btnVolver.TabIndex = 69;
            this.btnVolver.Text = "Volver al Menú Principal";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // dtBusquedaTurno
            // 
            this.dtBusquedaTurno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtBusquedaTurno.Location = new System.Drawing.Point(17, 184);
            this.dtBusquedaTurno.Name = "dtBusquedaTurno";
            this.dtBusquedaTurno.Size = new System.Drawing.Size(752, 187);
            this.dtBusquedaTurno.TabIndex = 70;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(480, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 72;
            this.label8.Text = "Número";
            // 
            // btnNumero
            // 
            this.btnNumero.Location = new System.Drawing.Point(676, 79);
            this.btnNumero.Name = "btnNumero";
            this.btnNumero.Size = new System.Drawing.Size(93, 23);
            this.btnNumero.TabIndex = 73;
            this.btnNumero.Text = "Buscar Numero";
            this.btnNumero.UseVisualStyleBackColor = true;
            this.btnNumero.Click += new System.EventHandler(this.btnNumero_Click);
            // 
            // txtNumeroTurno
            // 
            this.txtNumeroTurno.Location = new System.Drawing.Point(530, 81);
            this.txtNumeroTurno.Name = "txtNumeroTurno";
            this.txtNumeroTurno.Size = new System.Drawing.Size(130, 20);
            this.txtNumeroTurno.TabIndex = 74;
            // 
            // chSabado
            // 
            this.chSabado.AutoSize = true;
            this.chSabado.Location = new System.Drawing.Point(255, 511);
            this.chSabado.Name = "chSabado";
            this.chSabado.Size = new System.Drawing.Size(63, 17);
            this.chSabado.TabIndex = 81;
            this.chSabado.Text = "Sabado";
            this.chSabado.UseVisualStyleBackColor = true;
            // 
            // chViernes
            // 
            this.chViernes.AutoSize = true;
            this.chViernes.Location = new System.Drawing.Point(183, 511);
            this.chViernes.Name = "chViernes";
            this.chViernes.Size = new System.Drawing.Size(61, 17);
            this.chViernes.TabIndex = 80;
            this.chViernes.Text = "Viernes";
            this.chViernes.UseVisualStyleBackColor = true;
            // 
            // chJueves
            // 
            this.chJueves.AutoSize = true;
            this.chJueves.Location = new System.Drawing.Point(118, 510);
            this.chJueves.Name = "chJueves";
            this.chJueves.Size = new System.Drawing.Size(60, 17);
            this.chJueves.TabIndex = 79;
            this.chJueves.Text = "Jueves";
            this.chJueves.UseVisualStyleBackColor = true;
            // 
            // chMiercoles
            // 
            this.chMiercoles.AutoSize = true;
            this.chMiercoles.Location = new System.Drawing.Point(255, 486);
            this.chMiercoles.Name = "chMiercoles";
            this.chMiercoles.Size = new System.Drawing.Size(71, 17);
            this.chMiercoles.TabIndex = 78;
            this.chMiercoles.Text = "Miercoles";
            this.chMiercoles.UseVisualStyleBackColor = true;
            // 
            // chMartes
            // 
            this.chMartes.AutoSize = true;
            this.chMartes.Location = new System.Drawing.Point(183, 487);
            this.chMartes.Name = "chMartes";
            this.chMartes.Size = new System.Drawing.Size(58, 17);
            this.chMartes.TabIndex = 77;
            this.chMartes.Text = "Martes";
            this.chMartes.UseVisualStyleBackColor = true;
            // 
            // chLunes
            // 
            this.chLunes.AutoSize = true;
            this.chLunes.Location = new System.Drawing.Point(119, 487);
            this.chLunes.Name = "chLunes";
            this.chLunes.Size = new System.Drawing.Size(55, 17);
            this.chLunes.TabIndex = 76;
            this.chLunes.Text = "Lunes";
            this.chLunes.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 487);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 75;
            this.label3.Text = "Día:";
            // 
            // PantallaBuscarTurno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 540);
            this.Controls.Add(this.chSabado);
            this.Controls.Add(this.chViernes);
            this.Controls.Add(this.chJueves);
            this.Controls.Add(this.chMiercoles);
            this.Controls.Add(this.chMartes);
            this.Controls.Add(this.chLunes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNumeroTurno);
            this.Controls.Add(this.btnNumero);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dtBusquedaTurno);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNroTurno);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.txtHoraHasta);
            this.Controls.Add(this.txtHoraDesde);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnBuscarxDia);
            this.Controls.Add(this.btnBuscarxDesc);
            this.Controls.Add(this.txtBusquedaTurno2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtBusquedaTurno1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Name = "PantallaBuscarTurno";
            this.Text = "Buscar turno";
            
            ((System.ComponentModel.ISupportInitialize)(this.dtBusquedaTurno)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnBuscarxDia;
        private System.Windows.Forms.Button btnBuscarxDesc;
        private System.Windows.Forms.TextBox txtBusquedaTurno2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBusquedaTurno1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.TextBox txtHoraHasta;
        private System.Windows.Forms.TextBox txtHoraDesde;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNroTurno;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.DataGridView dtBusquedaTurno;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnNumero;
        private System.Windows.Forms.TextBox txtNumeroTurno;
        private System.Windows.Forms.CheckBox chSabado;
        private System.Windows.Forms.CheckBox chViernes;
        private System.Windows.Forms.CheckBox chJueves;
        private System.Windows.Forms.CheckBox chMiercoles;
        private System.Windows.Forms.CheckBox chMartes;
        private System.Windows.Forms.CheckBox chLunes;
        private System.Windows.Forms.Label label3;
    }
}
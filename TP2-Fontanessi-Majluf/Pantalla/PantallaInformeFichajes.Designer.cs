﻿namespace Pantalla
{
    partial class PantallaInformeFichajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNroLegajo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtFechaDesde = new System.Windows.Forms.DateTimePicker();
            this.dtFechaHasta = new System.Windows.Forms.DateTimePicker();
            this.dtFichajes = new System.Windows.Forms.DataGridView();
            this.btnBuscarNroLeg = new System.Windows.Forms.Button();
            this.btnBuscarFechas = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtFichajes)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número Legajo";
            // 
            // txtNroLegajo
            // 
            this.txtNroLegajo.Location = new System.Drawing.Point(140, 67);
            this.txtNroLegajo.Name = "txtNroLegajo";
            this.txtNroLegajo.Size = new System.Drawing.Size(149, 20);
            this.txtNroLegajo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fecha Desde";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Fecha Hasta";
            // 
            // dtFechaDesde
            // 
            this.dtFechaDesde.Location = new System.Drawing.Point(140, 102);
            this.dtFechaDesde.Name = "dtFechaDesde";
            this.dtFechaDesde.Size = new System.Drawing.Size(200, 20);
            this.dtFechaDesde.TabIndex = 4;
            // 
            // dtFechaHasta
            // 
            this.dtFechaHasta.Location = new System.Drawing.Point(140, 141);
            this.dtFechaHasta.Name = "dtFechaHasta";
            this.dtFechaHasta.Size = new System.Drawing.Size(200, 20);
            this.dtFechaHasta.TabIndex = 5;
            // 
            // dtFichajes
            // 
            this.dtFichajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtFichajes.Location = new System.Drawing.Point(58, 215);
            this.dtFichajes.Name = "dtFichajes";
            this.dtFichajes.Size = new System.Drawing.Size(505, 188);
            this.dtFichajes.TabIndex = 6;
            // 
            // btnBuscarNroLeg
            // 
            this.btnBuscarNroLeg.Location = new System.Drawing.Point(405, 62);
            this.btnBuscarNroLeg.Name = "btnBuscarNroLeg";
            this.btnBuscarNroLeg.Size = new System.Drawing.Size(141, 23);
            this.btnBuscarNroLeg.TabIndex = 7;
            this.btnBuscarNroLeg.Text = "Buscar por Número Legajo";
            this.btnBuscarNroLeg.UseVisualStyleBackColor = true;
            this.btnBuscarNroLeg.Click += new System.EventHandler(this.btnBuscarNroLeg_Click);
            // 
            // btnBuscarFechas
            // 
            this.btnBuscarFechas.Location = new System.Drawing.Point(405, 119);
            this.btnBuscarFechas.Name = "btnBuscarFechas";
            this.btnBuscarFechas.Size = new System.Drawing.Size(141, 23);
            this.btnBuscarFechas.TabIndex = 8;
            this.btnBuscarFechas.Text = "Buscar por Fechas";
            this.btnBuscarFechas.UseVisualStyleBackColor = true;
            this.btnBuscarFechas.Click += new System.EventHandler(this.btnBuscarFechas_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.Location = new System.Drawing.Point(562, 438);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(129, 23);
            this.btnVolver.TabIndex = 9;
            this.btnVolver.Text = "Volver al menú principal";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // PantallaInformeFichajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 476);
            this.Controls.Add(this.btnVolver);
            this.Controls.Add(this.btnBuscarFechas);
            this.Controls.Add(this.btnBuscarNroLeg);
            this.Controls.Add(this.dtFichajes);
            this.Controls.Add(this.dtFechaHasta);
            this.Controls.Add(this.dtFechaDesde);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNroLegajo);
            this.Controls.Add(this.label1);
            this.Name = "PantallaInformeFichajes";
            this.Text = "Informe Fichajes";
            ((System.ComponentModel.ISupportInitialize)(this.dtFichajes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNroLegajo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtFechaDesde;
        private System.Windows.Forms.DateTimePicker dtFechaHasta;
        private System.Windows.Forms.DataGridView dtFichajes;
        private System.Windows.Forms.Button btnBuscarNroLeg;
        private System.Windows.Forms.Button btnBuscarFechas;
        private System.Windows.Forms.Button btnVolver;
    }
}
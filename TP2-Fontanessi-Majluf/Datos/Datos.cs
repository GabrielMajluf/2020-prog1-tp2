﻿using Logica;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Datos
{
    public class Datos
    {
        //Ver si existen o crearlos
        public static void InicializarArchivos()
        {
            if (!File.Exists(@"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaAsistencia.txt"))
            {
                File.Create(@"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaAsistencia.txt").Close();
            }
            if (!File.Exists(@"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaEmpleado.txt"))
            {
                File.Create(@"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaEmpleado.txt").Close();
            }
            if (!File.Exists(@"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaTurno.txt"))
            {
                File.Create(@"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaTurno.txt").Close();
            }
        }

        // Lectura
        public static List<Empleado> LeerArchivoEmpleado()
        {
            string locationFile = @"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaEmpleado.txt";
            using (StreamReader r = new StreamReader(locationFile))
            {
                string content = r.ReadToEnd();
                List<Empleado> ListasEmpleado = JsonConvert.DeserializeObject<List<Empleado>>(content);
                return ListasEmpleado;
            }
        }
        public static List<Turno> LeerArchivoTurno()
        {
            string locationFile = @"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaTurno.txt";
            using (StreamReader r = new StreamReader(locationFile))
            {
                string content = r.ReadToEnd();
                List<Turno> ListasTurno = JsonConvert.DeserializeObject<List<Turno>>(content);
                return ListasTurno;
            }
        }
        public static List<Asistencia> LeerArchivoAsistencias()
        {
            string locationFile = @"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaAsistencia.txt";
            using (StreamReader r = new StreamReader(locationFile))
            {
                string content = r.ReadToEnd();
                List<Asistencia> ListasAsistencia = JsonConvert.DeserializeObject<List<Asistencia>>(content);
                return ListasAsistencia;
            }
        }

        // Guardado 
        public static void GuardarArhivoEmpleado(List<Empleado> ListaEmpleado)
        {
            string locationFile = @"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaEmpleado.txt";
            using (StreamWriter writer = new StreamWriter(locationFile, false))
            {
                string jsonContent = JsonConvert.SerializeObject(ListaEmpleado);
                writer.Write(jsonContent);
            }
        }
        public static void GuardarArhivoAsistencia(List<Asistencia> ListaAsistencia)
        {
            string locationFile = @"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaAsistencia.txt";
            using (StreamWriter writer = new StreamWriter(locationFile, false))
            {
                string jsonContent = JsonConvert.SerializeObject(ListaAsistencia);
                writer.Write(jsonContent);
            }
        }
        public static void GuardarArhivoTurno(List<Asistencia> ListaTurno)
        {
            string locationFile = @"C:\Users\pablo\OneDrive\Documents\2020-prog1-tp2\ListaTurno.txt";
            using (StreamWriter writer = new StreamWriter(locationFile, false))
            {
                string jsonContent = JsonConvert.SerializeObject(ListaTurno);
                writer.Write(jsonContent);
            }
        }
    }
}

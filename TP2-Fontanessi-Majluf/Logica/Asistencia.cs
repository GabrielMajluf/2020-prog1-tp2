﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Asistencia
    {
        public int NroTurnoAsisgnado { get; set; }
        public DateTime FechayHoraIngreso { get; set; }
        public string DiaSemana { get; set; }
        public DateTime FechayHoraSalida { get; set; }
        public int NroLegajoEmpleado { get; set; }
        public double CantMediasHsExtra { get; set; }
        public double CantTardanza { get; set; }
    }
}

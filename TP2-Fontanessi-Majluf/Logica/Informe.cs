﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Informe
    {
        public int NroLegajoEmpleadoI { get; set; }
        public string NombreEmpleado { get; set; }
        public int CantidadDiasTrabajados { get; set; }
        public double CantidadDiasAusente { get; set; }        
        public double CantidadHorasExtra { get; set; }
        public double CantidadDiasTardanza { get; set; }
    }
}

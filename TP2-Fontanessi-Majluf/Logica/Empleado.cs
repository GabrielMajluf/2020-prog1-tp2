﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado
    {
        public int Legajo { get; set; }
        public string Nombre { get; set; }
      //  public string Apellido { get; set; }
        public string Domicilio { get; set; }
        public string Localidad { get; set; }
        public int Telefono { get; set; }
        public string CorreoElectronico { get; set; }
        public float SueldoNeto { get; set; }
        public int Estado { get; set; }//(0: Activo, 99: Eliminado) Ver si no es mejor un bool que diga true: activo, false: eliminado
        public int TurnoAsignado { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaFinActividad { get; set; }
    }
}

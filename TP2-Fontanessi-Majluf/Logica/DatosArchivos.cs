﻿using Logica;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
   public class DatosArchivos
    {
        string RutaListaAsistencia = @"C:\Users\Gabri\Documents\2020-prog1-tp2\ListaAsistencia.txt";
        string RutaListaEmpleado = @"C:\Users\Gabri\Documents\2020-prog1-tp2\ListaEmpleado.txt";
        string RutaListaTurno = @"C:\Users\Gabri\Documents\2020-prog1-tp2\ListaTurno.txt";
        string RutaListaInforme = @"C:\Users\Gabri\Documents\2020-prog1-tp2\ListaInforme.txt";

        //Ver si existen o crearlos
        public void InicializarArchivos()
        {
            if (!File.Exists(RutaListaAsistencia))
            {
                File.Create(RutaListaAsistencia).Close();
            }
            if (!File.Exists(RutaListaEmpleado))
            {
                File.Create(RutaListaEmpleado).Close();
            }
            if (!File.Exists(RutaListaTurno))
            {
                File.Create(RutaListaTurno).Close();
            }
            if (!File.Exists(RutaListaInforme))
            {
                File.Create(RutaListaInforme).Close();
            }

        }

        // Lectura
        public List<Empleado> LeerArchivoEmpleado()
        {
            string locationFile = RutaListaEmpleado;
            using (StreamReader r = new StreamReader(locationFile))
            {
                string content = r.ReadToEnd();
                List<Empleado> ListasEmpleado = JsonConvert.DeserializeObject<List<Empleado>>(content);
                return ListasEmpleado;
            }
        }
        public  List<Turno> LeerArchivoTurno()
        {
            string locationFile = RutaListaTurno;
            using (StreamReader r = new StreamReader(locationFile))
            {
                string content = r.ReadToEnd();
                List<Turno> ListasTurno = JsonConvert.DeserializeObject<List<Turno>>(content);
                return ListasTurno;
            }
        }
        public  List<Asistencia> LeerArchivoAsistencias()
        {
            string locationFile = RutaListaAsistencia;
            using (StreamReader r = new StreamReader(locationFile))
            {
                string content = r.ReadToEnd();
                List<Asistencia> ListasAsistencia = JsonConvert.DeserializeObject<List<Asistencia>>(content);
                return ListasAsistencia;
            }
        }
        public List<Informe> LeerArchivoInforme()
        {
            string locationFile = RutaListaInforme;
            using (StreamReader r = new StreamReader(locationFile))
            {
                string content = r.ReadToEnd();
                List<Informe> ListasInformes = JsonConvert.DeserializeObject<List<Informe>>(content);
                return ListasInformes;
            }
        }


        // Guardado 
        public  void GuardarArhivoEmpleado(List<Empleado> ListaEmpleado)
        {
            string locationFile = RutaListaEmpleado;
            using (StreamWriter writer = new StreamWriter(locationFile, false))
            {
                string jsonContent = JsonConvert.SerializeObject(ListaEmpleado);
                writer.Write(jsonContent);
            }
        }
        public  void GuardarArhivoAsistencia(List<Asistencia> ListaAsistencia)
        {
            string locationFile = RutaListaAsistencia;
            using (StreamWriter writer = new StreamWriter(locationFile, false))
            {
                string jsonContent = JsonConvert.SerializeObject(ListaAsistencia);
                writer.Write(jsonContent);
            }
        }
        public  void GuardarArhivoTurno(List<Turno> ListaTurno)
        {
            string locationFile = RutaListaTurno;
            using (StreamWriter writer = new StreamWriter(locationFile, false))
            {
                string jsonContent = JsonConvert.SerializeObject(ListaTurno);
                writer.Write(jsonContent);
            }
        }
        public void GuardarArhivoInforme(List<Informe> ListaInforme)
        {
            string locationFile = RutaListaInforme;
            using (StreamWriter writer = new StreamWriter(locationFile, false))
            {
                string jsonContent = JsonConvert.SerializeObject(ListaInforme);
                writer.Write(jsonContent);
            }
        }
    }
}

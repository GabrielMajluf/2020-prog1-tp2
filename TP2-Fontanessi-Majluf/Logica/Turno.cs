﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Turno
    {
        public int Numero { get; set; }
        public string Descripcion { get; set; }
        public string DiaLunes { get; set; }
        public string DiaMartes { get; set; }
        public string DiaMiercoles { get; set; }
        public string DiaJueves { get; set; }
        public string DiaViernes { get; set; }
        public string DiaSabado{ get; set; }
        public double HoraDesde { get; set; }
        public double HoraHasta { get; set; }
        public int Estado { get; set; }
    }
}

﻿  using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        DatosArchivos ObjetoDatoArchivos = new DatosArchivos();
        
        List<Empleado> ListaEmpleados = new List<Empleado>();
                
        List<Turno> ListaTurnos = new List<Turno>();

        List<Asistencia> ListaAsistencias = new List<Asistencia>();
        List<Informe> ListaInformes = new List<Informe>();

        //Relleno de listas
        public void RellenarListas()
        {
            ListaAsistencias = ObjetoDatoArchivos.LeerArchivoAsistencias();
            ListaEmpleados = ObjetoDatoArchivos.LeerArchivoEmpleado();
            ListaTurnos = ObjetoDatoArchivos.LeerArchivoTurno();
            ListaInformes = ObjetoDatoArchivos.LeerArchivoInforme();
        }


        //EMPLEADOS
        public bool AgregarEmpleado(string Nombre, string Domicilio, string Localidad, int Telefono, string CorreoElectronico, float SueldoNeto,  int NroTurnoAsignado)
        {
            Empleado empleado = new Empleado();
            if (ValidarTurnoPorNro(NroTurnoAsignado) == true) //VER TEMA VALIDACIÓN TURNO --> De momento buscamos si existe, capaz se puede complejizar.
            {
                empleado.Legajo = ObtenerNroLegajo();
                empleado.Nombre = Nombre;
               // empleado.Apellido = Apellido;
                empleado.Domicilio = Domicilio;
                empleado.Localidad = Localidad;
                empleado.Telefono = Telefono;
                empleado.CorreoElectronico = CorreoElectronico;
                empleado.SueldoNeto = SueldoNeto;
                empleado.Estado = 0;
                empleado.TurnoAsignado = NroTurnoAsignado;
                empleado.FechaIngreso = DateTime.Today;
                //ListaEmpleados = ValidarContenidoListaEmpleado();
                ListaEmpleados.Add(empleado);
                ObjetoDatoArchivos.GuardarArhivoEmpleado(ListaEmpleados);
                return true;
            }
            else
                return false;
        }
       

        public bool ModificarEmpleado(int NroLegajo, string Nombre,  string Domicilio, string Localidad, int Telefono, string CorreoElectronico, float SueldoNeto, int NroTurnoAsignado,int Estado) //Sacar la parte de estado q no se tendria q cambiar por una mod solo en eliminacion
        {           
            if (ValidarTurnoPorNro(NroTurnoAsignado) == true)
            {
                foreach (var EmpleadoAModif in ListaEmpleados)
                {
                    if (EmpleadoAModif.Legajo == NroLegajo)
                    {
                        EmpleadoAModif.Nombre = Nombre;
                        EmpleadoAModif.Domicilio = Domicilio;
                        EmpleadoAModif.Localidad = Localidad;
                        EmpleadoAModif.Telefono = Telefono;
                        EmpleadoAModif.CorreoElectronico = CorreoElectronico;
                        EmpleadoAModif.Estado = Estado;
                        EmpleadoAModif.SueldoNeto = SueldoNeto;
                        EmpleadoAModif.TurnoAsignado = NroTurnoAsignado;
                    }
                    
                }
                ObjetoDatoArchivos.GuardarArhivoEmpleado(ListaEmpleados);
                return true;
            }
            else
            {
                return false;
            }           
        }

        public List<Empleado> ValidarContenidoListaEmpleado()
        {
            ListaEmpleados = ObjetoDatoArchivos.LeerArchivoEmpleado();
            if (ListaEmpleados == null)
            {
                List<Empleado> NuevaListaEmpleado = new List<Empleado>();
                return NuevaListaEmpleado;
            }
            else
            {
                return ListaEmpleados;
            }
        }

        public Empleado EncontrarEmpleadoporLegajo(int Legajo)
        {
            ListaEmpleados = ValidarContenidoListaEmpleado();
            var empleadosporLegajo = ListaEmpleados.Find(x => Legajo == x.Legajo);
            if (empleadosporLegajo.Estado != 99)
            {
                return empleadosporLegajo;
            }
            return null;
        }
        public List<Empleado> RellenarGridEmpleado(int Legajo)
        {
            ListaEmpleados = ValidarContenidoListaEmpleado();
            List<Empleado> ListaFiltrada = new List<Empleado>();
            //var ListaFiltrada = ListaEmpleados.FindAll(x => Legajo == x.Legajo);
            //return empleadosporLegajo;
            foreach (var empleado in ListaEmpleados)
            {
                if (empleado.Legajo.ToString().Contains(Legajo.ToString()))
                {
                    if (empleado.Estado != 99)
                    {
                        ListaFiltrada.Add(empleado);
                    }
                }
            }
            return ListaFiltrada;
        }

        public void EliminarEmpleadoPorLegajo(int Legajo)
        {
             ListaEmpleados = ValidarContenidoListaEmpleado();
            if (ValidarEmpleadoTrabajando(Legajo))
            {
                foreach (var empleado in ListaEmpleados)
                {
                    if (empleado.Legajo == Legajo)
                    {
                        empleado.Estado = 99;
                        empleado.FechaFinActividad = DateTime.Today;
                    }
                }
                ObjetoDatoArchivos.GuardarArhivoEmpleado(ListaEmpleados);
            }   
            ObjetoDatoArchivos.GuardarArhivoEmpleado(ListaEmpleados);            
        }

        public List<Empleado> EncontrarVariosEmpleadosporNombre(string Nombre)
        {
            List<Empleado> empleadosporNombre = new List<Empleado>();
            ListaEmpleados = ValidarContenidoListaEmpleado();
            foreach (var empleado in ListaEmpleados)
            {
                if (empleado.Nombre.Contains(Nombre))
                {
                    if (empleado.Estado != 99)
                    {
                        empleadosporNombre.Add(empleado);
                    }
                    
                }
            }
            return empleadosporNombre;
        }

        public int ObtenerNroLegajo()
        {
            int NroLegajo;
            ListaEmpleados = ValidarContenidoListaEmpleado();
            if (ListaEmpleados.Count()==0)
            {
                NroLegajo = 1;
            }
            else
            {
                NroLegajo = ListaEmpleados.Last().Legajo + 1;
            }
            return NroLegajo;
        }
        public Empleado EncontrarEmpleadoPorNombre(string Nombre)
        {
            ListaEmpleados = ValidarContenidoListaEmpleado();
            Empleado empleadovacio = new Empleado();
            foreach (var empleado in ListaEmpleados)
            {
                if (empleado.Nombre.Contains(Nombre))
                {
                    if (empleado.Estado != 99)
                    {
                        return empleado;
                    }                    
                }
            }
            return empleadovacio;
        }

        //TURNOS
        public void AgregarTurno(string Descripcion, string DiaLunes, string DiaMartes, string DiaMiercoles, string DiaJueves, string DiaViernes, string DiaSabado, double HoraDesde, double HoraHasta)
        {
            Turno turno = new Turno();

            turno.Numero = ObtenerNroTurno();
            turno.Descripcion = Descripcion;
            turno.DiaLunes = DiaLunes;
            turno.DiaMartes = DiaMartes;
            turno.DiaMiercoles = DiaMiercoles;
            turno.DiaJueves = DiaJueves;
            turno.DiaViernes = DiaViernes;
            turno.DiaSabado = DiaSabado;
            turno.HoraDesde = HoraDesde;
            turno.HoraHasta = HoraHasta;
            turno.Estado = 0;
            ListaTurnos = ValidarContenidoListaTurno();
            ListaTurnos.Add(turno);
            ObjetoDatoArchivos.GuardarArhivoTurno(ListaTurnos);
        }
        
        public List<Turno> ValidarContenidoListaTurno()
        {
            ListaTurnos = ObjetoDatoArchivos.LeerArchivoTurno();
            if (ListaTurnos == null)
            {
                List<Turno> NuevaListaTurno = new List<Turno>();
                return NuevaListaTurno;
            }
            else
            {
                return ListaTurnos;
            }
        }

        public bool ValidarTurnoPorNro(int Numero)
        {
            Turno TurnoExiste = new Turno();
            ListaTurnos = ValidarContenidoListaTurno();
            foreach (var turno in ListaTurnos)
            {
                if (turno.Numero == Numero)
                {
                    TurnoExiste = turno;
                }
            }
            if (TurnoExiste != null)
            {
                return true;
            }
            else
            {
                return false;
            }               
        }
        
        public int ObtenerNroTurno()
        {
            int NroTurno;
            ListaTurnos = ValidarContenidoListaTurno();
            if (ListaTurnos.Count == 0)
            {
                NroTurno = 1;
            }
            else
            {
                NroTurno = ListaTurnos.Last().Numero + 1;
            }
            return NroTurno;
        }

        public Turno BuscarTurnoPorNro(int Numero)
        {
            ListaTurnos = ValidarContenidoListaTurno();
            var turnosporNumero = ListaTurnos.Find(x => Numero == x.Numero);
            return turnosporNumero;
        }

        public Turno EncontrarTurnoporDescripcion(string Descripcion)
        {
            ListaTurnos = ValidarContenidoListaTurno();
            var turnosporDescripcion = ListaTurnos.Find(x => Descripcion == x.Descripcion);
            return turnosporDescripcion;
        }

        public Turno EncontrarTurnoporDia(string Dia)
        {
            ListaTurnos = ValidarContenidoListaTurno();
            Turno turnosporDia = new Turno();
            switch (Dia)
            {
                case "Monday":
                    turnosporDia = ListaTurnos.Find(x => Dia == x.DiaLunes);
                    break;
                case "Tuesday":
                     turnosporDia = ListaTurnos.Find(x => Dia == x.DiaMartes);
                    break;
                case "Wednesday":
                    turnosporDia = ListaTurnos.Find(x => Dia == x.DiaMiercoles);
                    break;
                case "Thursday":
                    turnosporDia = ListaTurnos.Find(x => Dia == x.DiaJueves);
                    break;
                case "Friday":
                    turnosporDia = ListaTurnos.Find(x => Dia == x.DiaViernes);
                    break;
                case "Saturday":
                    turnosporDia = ListaTurnos.Find(x => Dia == x.DiaSabado);
                    break;
                default:
                    break;
            }
            return turnosporDia;
        }

        public List<Turno> EncontrarVariosTurnosporDescripcion(string Descripcion)
        {            
            ListaTurnos = ValidarContenidoListaTurno();
            List<Turno> turnosporDescripcion = new List<Turno>();

            foreach (var turno in ListaTurnos)
            {
                if (turno.Descripcion.Contains(Descripcion))
                {

                    turnosporDescripcion.Add(turno);
                }
            }        
            return turnosporDescripcion;                        
        }

        public List<Turno> EncontrarVariosTurnosporDia(string Dia)
        {
            List<Turno> turnosporDia = new List<Turno>();
            ListaTurnos = ValidarContenidoListaTurno();
            foreach (var turno in ListaTurnos)
            {
                if (turno.DiaLunes.Contains(Dia) | turno.DiaMartes.Contains(Dia) | turno.DiaMiercoles.Contains(Dia) | turno.DiaJueves.Contains(Dia) | turno.DiaViernes.Contains(Dia) | turno.DiaSabado.Contains(Dia))
                {
                    turnosporDia.Add(turno);
                }
            }
            return turnosporDia;
        }

        public bool ModificarTurno(int Numero, string Descripcion, string DiaLunes, string DiaMartes, string DiaMiercoles, string DiaJueves, string DiaViernes, string DiaSabado, double HoraDesde, double HoraHasta)
        {
            foreach (var TurnoAModif in ListaTurnos)
            {
                if (TurnoAModif.Numero== Numero)
                {
                    TurnoAModif.Descripcion = Descripcion;
                    TurnoAModif.DiaLunes = DiaLunes;
                    TurnoAModif.DiaMartes = DiaMartes;
                    TurnoAModif.DiaMiercoles = DiaMiercoles;
                    TurnoAModif.DiaJueves = DiaJueves;
                    TurnoAModif.DiaViernes = DiaViernes;
                    TurnoAModif.DiaSabado = DiaSabado;
                    TurnoAModif.HoraDesde = HoraDesde;
                    TurnoAModif.HoraHasta = HoraHasta;
                    TurnoAModif.Estado = 0;
                }
                
                ObjetoDatoArchivos.GuardarArhivoTurno(ListaTurnos);
            }
            ObjetoDatoArchivos.GuardarArhivoTurno(ListaTurnos);
            return true;               
        }
        public void EliminarTurnoPorNumero(int Numero)
        {
            ListaTurnos = ValidarContenidoListaTurno();
            if (ValidarNoTengaEmpleados(Numero))
            {
                foreach (var turno in ListaTurnos)
                {
                    if (turno.Numero == Numero)
                    {
                        turno.Estado = 99;
                    }
                }
                ObjetoDatoArchivos.GuardarArhivoTurno(ListaTurnos);
            }
            ObjetoDatoArchivos.GuardarArhivoTurno(ListaTurnos);
        }
        public bool ValidarNoTengaEmpleados(int nroTurno)
        {
            ListaEmpleados = ValidarContenidoListaEmpleado();

            foreach (var empleado in ListaEmpleados)
            {
                if (empleado.TurnoAsignado == nroTurno)
                {
                    return false;
                }
            }
            return true;
        }

        // ASISTENCIAS

        public bool RegistrarIngresoEmpleado(DateTime DatoAsistencia, Empleado empleado)
        {
            Asistencia NuevaAsistencia = new Asistencia();
          ////  Turno DatoTurno = new Turno(); //Para calcular tardanza
            
            string MarcaNoRegistroEgreso = "19000101";
            DateTime  FechaSinRegistroEgreso;
            if (DateTime.TryParseExact(MarcaNoRegistroEgreso, "yyyyMMdd", CultureInfo.InvariantCulture, 
                DateTimeStyles.None, out FechaSinRegistroEgreso))
            {
                NuevaAsistencia.FechayHoraSalida = FechaSinRegistroEgreso; 
            }
           
            if (VerificarIngreso(DatoAsistencia, empleado))
            {
                NuevaAsistencia.NroLegajoEmpleado = empleado.Legajo;
                NuevaAsistencia.FechayHoraIngreso = DatoAsistencia;
                NuevaAsistencia.NroTurnoAsisgnado = empleado.TurnoAsignado;
                NuevaAsistencia.DiaSemana = DatoAsistencia.DayOfWeek.ToString();
                double Diferencia =  (BuscarTurnoPorNro(NuevaAsistencia.NroTurnoAsisgnado)).HoraDesde - DatoAsistencia.Hour;
                double MinDif = Diferencia * 60;
                if (Diferencia > 0) // es decir, que llego temprano
                {
                    if (MinDif / 30 > 1 | MinDif / 30 < -1)
                    {
                        NuevaAsistencia.CantMediasHsExtra = Math.Truncate(Math.Abs(MinDif / 30));
                    }
                    else
                    {
                        if (MinDif / 30 == 1 | MinDif / 30 == -1)
                        {
                            NuevaAsistencia.CantMediasHsExtra = 1;
                        }
                    }
                }
                else
                {
                    if (Diferencia < 0) // es decir que llego tarde
                    {
                        if (MinDif / 15 > 1 | MinDif / 15 < -1)
                        {
                            NuevaAsistencia.CantTardanza = Math.Truncate(Math.Abs(MinDif / 15));
                        }
                        else
                        {
                            if (MinDif / 15 == 1 | MinDif / 15 == -1)
                            {
                                NuevaAsistencia.CantTardanza = 1;
                            }
                        }
                    }
                }
                ListaAsistencias.Add(NuevaAsistencia);
                ObjetoDatoArchivos.GuardarArhivoAsistencia(ListaAsistencias);           
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public bool VerificarIngreso(DateTime DatoAsistencia, Empleado empleado)
        {
            ListaTurnos = ValidarContenidoListaTurno();
            
            foreach (var turno in ListaTurnos)
            {
                if (turno.Numero == empleado.TurnoAsignado)
                {
                    
                    switch (DatoAsistencia.DayOfWeek.ToString())
                    {
                        case "Monday":
                            if (turno.DiaLunes != "")
                            {
                                return true;
                            }
                            break;
                        case "Tuesday":
                            if (turno.DiaMartes != "")
                            {
                                return true;
                            }
                            break;
                        case "Wednesday":
                            if (turno.DiaMiercoles != "")
                            {
                                return true;
                            }
                            break;
                        case "Thursday":
                            if (turno.DiaJueves != "")
                            {
                                return true;
                            }
                            break;
                        case "Friday":
                            if (turno.DiaViernes != "")
                            {
                                return true;
                            }
                            break;
                        case "Saturday":
                            if (turno.DiaSabado != "")
                            {
                                return true;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            return false;
        }

        public bool RegistrarEgresoEmpleado(DateTime DatoAsistencia, Empleado empleado)
        {
            string MarcaNoRegistroEgreso = "19000101";
            DateTime FechaSinRegistroEgreso;
            DateTime.TryParseExact(MarcaNoRegistroEgreso, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaSinRegistroEgreso);
            ListaAsistencias = ValidaContenidoListaAsistencias();
            if (VerificarNoHizoOtroEgreso(DatoAsistencia, empleado))
            {
                foreach (var asistencia in ListaAsistencias)
                {
                    if (asistencia.NroLegajoEmpleado == empleado.Legajo)
                    {
                        if (asistencia.FechayHoraSalida == FechaSinRegistroEgreso)
                        {
                            asistencia.FechayHoraSalida = DatoAsistencia;
                            double Diferencia = (BuscarTurnoPorNro(asistencia.NroTurnoAsisgnado)).HoraHasta - DatoAsistencia.Hour;
                            double MinDif = Diferencia * 60;
                            if (Diferencia < 0) // es decir, que se va tarde
                            {
                                if (MinDif / 30 > 1 | MinDif / 30 < -1)
                                {
                                    asistencia.CantMediasHsExtra += Math.Truncate(Math.Abs(MinDif / 30));
                                }
                                else
                                {
                                    if (MinDif / 30 == 1 | MinDif / 30 == -1)
                                    {
                                        asistencia.CantMediasHsExtra += 1;
                                    }
                                }
                            }
                            else
                            {
                                if (Diferencia > 0) // es decir, que se va temprano
                                {
                                    if (MinDif / 15 > 1 | MinDif / 15 < -1)
                                    {
                                        asistencia.CantTardanza += Math.Truncate(Math.Abs(MinDif / 15));
                                    }
                                    else
                                    {
                                        if (MinDif / 15 == 1 | MinDif / 15 == -1)
                                        {
                                            asistencia.CantTardanza += 1;
                                        }
                                    }
                                }
                            }
                            ObjetoDatoArchivos.GuardarArhivoAsistencia(ListaAsistencias);
                        }
                    }
                }
                ObjetoDatoArchivos.GuardarArhivoAsistencia(ListaAsistencias);
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Asistencia> ValidaContenidoListaAsistencias()
        {
            ListaAsistencias= ObjetoDatoArchivos.LeerArchivoAsistencias();
            if (ListaAsistencias == null)
            {
                List<Asistencia> NuevaListaaAsistencia = new List<Asistencia>();
                return NuevaListaaAsistencia;
            }
            else
            {
                return ListaAsistencias;
            }
        }

        public bool VerificarNoHizoOtroEgreso(DateTime DatoAsistencia, Empleado empleado)
        {
            string MarcaNoRegistroEgreso = "19000101";
            DateTime FechaSinRegistroEgreso;
            DateTime.TryParseExact(MarcaNoRegistroEgreso, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaSinRegistroEgreso);
            ListaAsistencias = ValidaContenidoListaAsistencias();
            foreach (var asistencia in ListaAsistencias)
            {
                if (asistencia.NroLegajoEmpleado == empleado.Legajo)
                {
                    if (asistencia.FechayHoraSalida == FechaSinRegistroEgreso)
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public List<Asistencia> BuscarListadoAsistenciaEmpleadoPorNroLegajo(int NroLegajo)
        {
            ListaAsistencias = ValidaContenidoListaAsistencias();
            List<Asistencia> ListaFiltrada = new List<Asistencia>();
            foreach (var asistencia in ListaAsistencias)
            {
                if (asistencia.NroLegajoEmpleado == NroLegajo)
                {
                    ListaFiltrada.Add(asistencia);
                }
            }
            return ListaFiltrada;
        }
        public List<Asistencia> BuscarListadoAsistenciaEmpleadoPorFechas(int NroLegajo, DateTime FechaDesde, DateTime FechaHasta)
        {
            ListaAsistencias = ValidaContenidoListaAsistencias();
            List<Asistencia> ListaFiltrada = new List<Asistencia>();
            foreach (var asistencia in ListaAsistencias)
            {
                if (asistencia.NroLegajoEmpleado == NroLegajo)
                {
                    if (asistencia.FechayHoraIngreso >= FechaDesde & asistencia.FechayHoraIngreso <= FechaHasta)
                    {
                        ListaFiltrada.Add(asistencia);
                    }                    
                }
            }
            return ListaFiltrada;
        }
        public bool ValidarEmpleadoTrabajando(int NroLegajo)
        {
            string MarcaNoRegistroEgreso = "19000101";
            DateTime FechaSinRegistroEgreso;
            DateTime.TryParseExact(MarcaNoRegistroEgreso, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaSinRegistroEgreso);

            ListaAsistencias = ValidaContenidoListaAsistencias();
            foreach (var asistencia in ListaAsistencias)
            {
                if (asistencia.NroLegajoEmpleado == NroLegajo & asistencia.FechayHoraSalida == FechaSinRegistroEgreso)
                {
                    return false;
                }
            }
            return true;
        }
        //INFORMES

        public List<Informe> ValidarContenidoListaInforme()
        {
            ListaInformes = ObjetoDatoArchivos.LeerArchivoInforme();
            if (ListaInformes == null)
            {
                List<Informe> NuevaListaInforme = new List<Informe>();
                return NuevaListaInforme;
            }
            else
            {
                return ListaInformes;
            }
        }

        public List<Informe> InformarEmpleados()
        {
            List<Informe>ListaInformes = new List<Informe>();
            ListaEmpleados = ValidarContenidoListaEmpleado();
            foreach (var empleado in ListaEmpleados)
            {   
                Informe informeEmpleado = new Informe();
                informeEmpleado.NroLegajoEmpleadoI = empleado.Legajo;
                informeEmpleado.NombreEmpleado = EncontrarEmpleadoporLegajo(empleado.Legajo).Nombre;
                informeEmpleado.CantidadDiasTrabajados = ObtenerCantDiasTrabajados(empleado.Legajo);
                informeEmpleado.CantidadHorasExtra = ObtenerCantHorasExtra(empleado.Legajo);
                informeEmpleado.CantidadDiasTardanza = ObtenerCantTardanzas(empleado.Legajo);
                informeEmpleado.CantidadDiasAusente = CantDiasInasistencias(empleado.Legajo);
                ListaInformes.Add(informeEmpleado);
                
            }
            
            return ListaInformes;
        }
        public int ObtenerCantDiasTrabajados(int nroLegajo)
        {
            int CantDiasTrabajados = 0;
            foreach (var asistencia in ListaAsistencias)
            {
                if (asistencia.NroLegajoEmpleado == nroLegajo)
                {
                    CantDiasTrabajados += 1;
                } 
            }
            return CantDiasTrabajados;
        }
        public double ObtenerCantHorasExtra(int nroLegajo)
        {
            double CantHorasExtra = 0;
            ListaAsistencias = ValidaContenidoListaAsistencias();
            foreach (var asistencia in ListaAsistencias)
            {
                if (asistencia.NroLegajoEmpleado == nroLegajo)
                {
                    CantHorasExtra += Math.Truncate(asistencia.CantMediasHsExtra);
                }
            }
            double CantTardanzas = ObtenerCantTardanzas(nroLegajo);
            CantHorasExtra = CantHorasExtra - CantTardanzas; //Restamos las tardanzas que restan horas extras.
            return Math.Truncate(CantHorasExtra / 2); // Dividimos por dos pq contamos Medias horas en asistencia, por ende para obtener las hs dividimos por 2
        }
        public double ObtenerCantTardanzas(int nroLegajo)
        {
            ListaAsistencias = ValidaContenidoListaAsistencias();
            double CantTardanza = 0;
            foreach (var asistencia in ListaAsistencias)
            {
                if (asistencia.NroLegajoEmpleado == nroLegajo)
                {
                    CantTardanza += Math.Truncate(asistencia.CantTardanza);
                }
            }
            return Math.Truncate(CantTardanza / 4); //Dividimos por 4 pq contamos 15 min de tardanza, por ende para obtener las hs de tardanza dividimos por 4
        }
        public Double CantDiasInasistencias(int NroLegajo)
        {
           
            Empleado empleado = EncontrarEmpleadoporLegajo(NroLegajo);
           
            double CantidadDiasEnLaEmpresa;
            if (empleado.FechaFinActividad != null)
            {
                CantidadDiasEnLaEmpresa = Math.Truncate((empleado.FechaFinActividad - empleado.FechaIngreso).TotalDays);
                if (CantidadDiasEnLaEmpresa <= 0)
                {
                    return 0;
                }
            }
            else
            {
                CantidadDiasEnLaEmpresa = Math.Truncate((DateTime.Today - empleado.FechaIngreso).TotalDays);
                if (CantidadDiasEnLaEmpresa <= 0)
                {
                    return 0;
                }
            }
            double CantSemanasTrabajo = CantidadDiasEnLaEmpresa / 7;
            double CantDiasDebioTrabajar = CantSemanasTrabajo * (ObtenerCantDiasTrabajaTurno(empleado.TurnoAsignado));

            if (CantDiasDebioTrabajar < ObtenerCantDiasTrabajados(empleado.Legajo))
            {
                return Math.Truncate(CantDiasDebioTrabajar - ObtenerCantDiasTrabajados(empleado.Legajo));
            }
            else
            {
                return 0;
            }
        
        }

        private double ObtenerCantDiasTrabajaTurno(int turnoAsignado)
        {
            Turno turno = BuscarTurnoPorNro(turnoAsignado);
            int CantDiasTrabajaPorSemana = 0;
            if (turno.DiaLunes != "")
            {
                CantDiasTrabajaPorSemana += 1;
            }
            if (turno.DiaMartes !="")
            {
                CantDiasTrabajaPorSemana += 1;
            }
            if (turno.DiaMiercoles != "")
            {
                CantDiasTrabajaPorSemana += 1;
            }
            if (turno.DiaJueves != "")
            {
                CantDiasTrabajaPorSemana += 1;
            }
            if (turno.DiaViernes != "")
            {
                CantDiasTrabajaPorSemana += 1;
            }
            if (turno.DiaSabado != "")
            {
                CantDiasTrabajaPorSemana += 1;
            }
            return CantDiasTrabajaPorSemana;
        }
    }
}

